const bdd = [
  {
    id: 0,
    name: "user1",
    password: "123",
    events: [
      {
        id: 0,
        name: "event 1",
        date: 12312314
      }
    ]
  }
];

function generateUserId() {
  const ids = bdd.map(user => user.id);
  return Math.max(...ids) + 1;
}

function createUser(name, password) {
  const user = {
    id: generateUserId(),
    name,
    password,
    events: []
  };
  bdd.push(user);
  return user;
}

function getUser(name) {
  return bdd.find(user => user.name === name);
}

function generateEventId() {
  const events = bdd.reduce((res, cur) => {
    return res.concat(cur.events);
  }, []);
  const ids = events.map(user => user.id);
  return Math.max(...ids) + 1;
}

function createEvent(userId, name, date) {
  const event = {
    id: generateEventId(),
    name,
    date
  };
  const user = bdd.find(user => user.id === +userId);
  user.events.push(event);
}

function getEventsByUser(userId) {
  const user = bdd.find(user => user.id === +userId);
  return user.events;
}

function getEventById(userId, eventId) {
  const events = getEventsByUser(userId);
  return events.find(event => event.id === +eventId);
}

function deleteEvent(userId, eventId) {
  const user = bdd.find(user => user.id === +userId);
  user.events = user.events.filter(event => event.id !== +eventId);
}

module.exports = {
  bdd,
  createUser,
  createEvent,
  getEventsByUser,
  getUser,
  getEventById,
  deleteEvent
};
