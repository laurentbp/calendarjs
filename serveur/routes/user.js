const express = require("express");
const jwt = require("jsonwebtoken");
const bdd = require("../bdd");

const router = express.Router();

router.post("/signup", function(req, res) {
  const name = req.body.name;
  const mdp = req.body.mdp;

  if (!name || !mdp) {
    res.status(401).json({ error: "Nom ou mot de passe manquant" });
    return;
  }

  const user = bdd.createUser(name, mdp);

  res.status(200).json({
    message: "utilisateur créé avec succes",
    user
  });
});

router.post("/login", function(req, res) {
  const name = req.body.name;
  const mdp = req.body.mdp;

  if (!name || !mdp) {
    res.status(401).json({ error: "Nom ou mot de passe manquant" });
    return;
  }

  const user = bdd.getUser(name);

  if (!user || user.password !== mdp) {
    res.status(401).json({ error: "Login échoué" });
    return;
  }

  const JWT = jwt.sign({ id: user.id, user: user.name }, "ILoveRakoune34");

  res.status(200).json({
    message: "Login effectué",
    jwt: JWT
  });
});

module.exports = router;
