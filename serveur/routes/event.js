const express = require("express");
const passport = require("../jwt");
const bdd = require("../bdd");

const router = express.Router();

router.get("/:id", passport, function(req, res) {
  const event = bdd.getEventById(req.user.id, req.params.id);

  res.status(200).json({
    event
  });
});

router.get("/", passport, function(req, res) {
  const events = bdd.getEventsByUser(req.user.id);

  res.status(200).json({
    events
  });
});

router.post("/", passport, function(req, res) {
  const name = req.body.name;
  const date = req.body.date;

  if (!name || !date) {
    res.status(401).json({ error: "nom ou date manquante" });
    return;
  }

  bdd.createEvent(req.user.id, name, +date);

  res.status(200).json({
    message: "event ajouté"
  });
});

router.delete("/:id", passport, function(req, res) {
  bdd.deleteEvent(req.user.id, req.params.id);
  res.status(200).json({
    message: "event supprimé"
  });
});

module.exports = router;
