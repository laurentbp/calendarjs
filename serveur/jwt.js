const passport = require("passport");
const passportJWT = require("passport-jwt");

const bdd = require("./bdd");

const ExtractJwt = passportJWT.ExtractJwt;
const JwtStrategy = passportJWT.Strategy;

const jwtOptions = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: "ILoveRakoune34"
};

const jwtStrategy = new JwtStrategy(jwtOptions, function(payload, next) {
  const user = bdd.getUser(payload.user);

  if (user) {
    next(null, user);
  } else {
    next(null, false);
  }
});

passport.use(jwtStrategy);

module.exports = passport.authenticate("jwt", { session: false });
