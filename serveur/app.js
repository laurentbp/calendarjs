const express = require("express");
const cors = require("cors");
const PORT = process.env.PORT || 3000;

const app = express();

app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(cors({ credentials: true, origin: true }));
app.options("*", cors());

const eventRoutes = require("./routes/event");
const userRoutes = require("./routes/user");

app.use("/events", eventRoutes);
app.use("/users", userRoutes);

app.listen(PORT, () => {
  console.log("démarrage sur le port " + PORT);
});
