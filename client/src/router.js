import Vue from "vue";
import Router from "vue-router";
import Home from "./pages/Home";
import Event from "./pages/Event";
import AddEvent from "./pages/AddEvent";

Vue.use(Router);

export default new Router({
  routes: [
    {
      component: Home,
      path: "/"
    },
    {
      component: AddEvent,
      path: "/event/add"
    },
    {
      component: Event,
      path: "/event/:id"
    }
  ]
});
